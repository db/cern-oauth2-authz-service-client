FROM jboss/wildfly
ADD target/cern-oauth2-authz-service-client-1.0.war /opt/jboss/wildfly/standalone/deployments/
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0","--debug", "8000"]
