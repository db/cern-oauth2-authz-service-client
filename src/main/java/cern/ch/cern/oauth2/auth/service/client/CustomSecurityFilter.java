/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.cern.oauth2.auth.service.client;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author lurodrig
 */
@WebFilter(filterName = "CustomSecurityFilter", urlPatterns = {"/*"}, initParams = {
    @WebInitParam(name = "COOKIE_NAME", value = "MY_SUPER_SECRET_COOKIE")
    ,
    @WebInitParam(name = "OAUTH2_CLIENT_URL", value = "/client_credentials_grant_redirect")})
public class CustomSecurityFilter implements Filter {

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;
    private static final boolean debug = true;
    private static final String COOKIE_NAME = "COOKIE_NAME";
    private static final String OAUTH2_CLIENT_URL = "OAUTH2_CLIENT_URL";

    public CustomSecurityFilter() {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        try {
            // Look for the COOKIE_NAME cookie
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            Cookie[] cookies = httpServletRequest.getCookies();
            String secretCookieHeader = null;
            // There should not be thousand of cookies, so no need for complex search algorithms :)
            if (cookies != null && cookies.length > 0) {
                for (Cookie cookie : cookies) {
                    if (System.getenv(COOKIE_NAME).equals(cookie.getName())) {
                        secretCookieHeader = cookie.getValue();
                        break;
                    }
                }
            }
            String secretCookieSession = (String) httpServletRequest.getSession().getAttribute(System.getenv(COOKIE_NAME));
            // If COOKIE_NAME is present means that user has been authenticated and has a valid session
            if (secretCookieSession != null && secretCookieSession.equals(secretCookieHeader)) {
                chain.doFilter(request, response);
            } else {
                // If both parameters are present in the requests means that we are in the callback response from CERN OUATH2 AUTHZ SERVICE
                if (httpServletRequest.getParameter(Constants.CODE) == null && httpServletRequest.getParameter(Constants.STATE) == null) {
                    // Store original request in session
                    String return_path = httpServletRequest.getRequestURL().toString();
                    if (httpServletRequest.getQueryString() != null) {
                        return_path = return_path + "?" + httpServletRequest.getQueryString();
                    }
                    httpServletRequest.getSession().setAttribute(Constants.RETURN_PATH, return_path);
                }
                // Forward this request to our OUATH2 client servlet, it knows what to do with it.
                httpServletRequest.getRequestDispatcher(filterConfig.getInitParameter(OAUTH2_CLIENT_URL)).forward(request, response);
            }
        } catch (Throwable t) {
            // If an exception is thrown somewhere down the filter chain,
            // we still want to execute our after processing, and then
            // rethrow the problem after that.
            t.printStackTrace();
        }
    }

    /**
     * Init method for this filter
     */
    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {
                log("CustomSecurityFilter:Initializing filter");
            }
        }
    }

    @Override
    public void destroy() {
    }

    private void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }

}
