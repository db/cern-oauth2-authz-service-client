/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.cern.oauth2.auth.service.client.http;

import cern.ch.cern.oauth2.auth.service.client.Constants;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 *
 * @author lurodrig
 */
public class HttpClientUtils {

    public static Form createForm(MultivaluedMap<String, String> params) {
        Form form = new Form(params);
        return form;
    }

    public static String getResponseBody(Form form, String url) {
        Entity<Form> data = Entity.form(form);
        Response response = ClientBuilder.newClient().target(url).request().post(data);
        return response.readEntity(String.class);
    }

    public static Client newBearerAuthenticatorClient(String token) {
        return ClientBuilder.newClient().register(new BearerAuthenticator(token));
    }

    public static String getResponse(Client client, String url) {
        Response response = client.target(url).request().get();
        return getResponseBodyAsString(response);
    }

    private static String getResponseBodyAsString(Response response) {
        String responseBody = response.readEntity(String.class);
        return responseBody;
    }
}
