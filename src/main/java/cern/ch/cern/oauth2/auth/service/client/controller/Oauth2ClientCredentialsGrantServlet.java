/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.cern.oauth2.auth.service.client.controller;

import cern.ch.cern.oauth2.auth.service.client.Constants;
import cern.ch.cern.oauth2.auth.service.client.api.Oauth2ServiceAPI;
import cern.ch.cern.oauth2.auth.service.client.api.Oauth2ServiceAPImpl;
import cern.ch.cern.oauth2.auth.service.client.grant.Oauth2ClientCredentialsClient;
import cern.ch.cern.oauth2.auth.service.client.http.HttpClientUtils;
import cern.ch.cern.oauth2.auth.service.client.json.JsonUtils;
import java.io.IOException;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

/**
 *
 * @author lurodrig
 */
public class Oauth2ClientCredentialsGrantServlet extends HttpServlet {

    private static final String REDIRECT_URI = "redirect_uri";
    private static final String GRANT_TYPE = "grant_type";
    private static final String AUTHORIZATION_CODE = "authorization_code";
    private static final String CLIENT_SECRET = "client_secret";
    private static final String CLIENT_ID = "client_id";
    private static final String ACCESS_TOKEN = "access_token";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Oauth2ClientCredentialsClient oauth2ClientCredentialsClient = Oauth2ClientCredentialsClient.getInstance();
        HttpSession session = request.getSession();
        String return_path = new String();
        // If there is state attribute in session means that the user has already been redirected to the AUTHZ_CODE_URL   
        if (session.getAttribute(Constants.STATE) != null) {
            // Check if there is a state parameter and if its value is the same as the one stored in session
            if (request.getParameter(Constants.STATE) != null && session.getAttribute(Constants.STATE).equals(request.getParameter(Constants.STATE))) {
                // state parameter is a "nonce", this is a value that may be used once. Delete it from session
                session.removeAttribute(Constants.STATE);
                String code = request.getParameter(Constants.CODE);
                String accessToken = getAccessToken(oauth2ClientCredentialsClient, code);
                Oauth2ServiceAPI oauth2ServiceAPI = new Oauth2ServiceAPImpl();
                session.setAttribute(Constants.AUTHENTICATED_USER, oauth2ServiceAPI.getUserWithGroups(accessToken));
                String secretCookieValue = UUID.randomUUID().toString();
                session.setAttribute(System.getenv(Constants.COOKIE_NAME), secretCookieValue);
                Cookie cookie = new Cookie(System.getenv(Constants.COOKIE_NAME), secretCookieValue);
                response.addCookie(cookie);
                return_path = (String) session.getAttribute(Constants.RETURN_PATH);
                if (return_path != null) {
                    session.removeAttribute(Constants.RETURN_PATH);
                    response.sendRedirect(return_path);
                }
            }
        } else {
            // This part of the code should be invoked just one per session. 
            // state parameter helps to mitigate CSRF attacks.
            String state = UUID.randomUUID().toString();
            session.setAttribute(Constants.STATE, state);
            // Redirect the user to AUTHZ_CODE_URL and use state to validate the callback
            response.sendRedirect(oauth2ClientCredentialsClient.getAuthzCodeURL() + state + "&" + Constants.RETURN_PATH + "=" + return_path);
        }
    }

    private String getAccessToken(Oauth2ClientCredentialsClient oauth2ClientCredentialsClient, String code) {
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(GRANT_TYPE, AUTHORIZATION_CODE);
        params.add(Constants.CODE, code);
        params.add(CLIENT_SECRET, oauth2ClientCredentialsClient.getClient_secret());
        params.add(CLIENT_ID, oauth2ClientCredentialsClient.getClient_id());
        params.add(REDIRECT_URI, oauth2ClientCredentialsClient.getRedirect_uri());
        String responseBody = HttpClientUtils.getResponseBody(HttpClientUtils.createForm(params), oauth2ClientCredentialsClient.getAuthzTokenURL());
        return JsonUtils.get(responseBody, ACCESS_TOKEN);
    }
}
