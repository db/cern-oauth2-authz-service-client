/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.cern.oauth2.auth.service.client.user;

import java.util.List;

/**
 *
 * @author lurodrig
 */
public class UserWithGroups extends User {
    
    private List<String> groups;

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return super.toString() + " UserWithGroups{" + "groups=" + groups + '}';
    }
}
