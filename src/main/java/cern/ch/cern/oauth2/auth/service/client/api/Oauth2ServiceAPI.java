/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.cern.oauth2.auth.service.client.api;

import cern.ch.cern.oauth2.auth.service.client.user.UserWithGroups;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 *
 * @author lurodrig
 */
public interface Oauth2ServiceAPI {
    
    public JsonArray getMe();
    public JsonObject getUser();
    public JsonArray getGroups();
    public UserWithGroups getUserWithGroups(String accessToken);
}
