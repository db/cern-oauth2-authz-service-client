/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.cern.oauth2.auth.service.client.api;

import cern.ch.cern.oauth2.auth.service.client.http.HttpClientUtils;
import cern.ch.cern.oauth2.auth.service.client.user.UserFactory;
import cern.ch.cern.oauth2.auth.service.client.user.UserWithGroups;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;

/**
 *
 * @author lurodrig
 */
public class Oauth2ServiceAPImpl implements Oauth2ServiceAPI {

    private static final String USER_URL = "USER_URL";
    private static final String GROUPS_URL = "GROUPS_URL";

    @Override
    public JsonArray getMe() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public JsonObject getUser() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public JsonArray getGroups() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UserWithGroups getUserWithGroups(String accessToken) {
        Client client = HttpClientUtils.newBearerAuthenticatorClient(accessToken);
        String userAsString = HttpClientUtils.getResponse(client, System.getenv(USER_URL));
        String groupsAsString = HttpClientUtils.getResponse(client, System.getenv(GROUPS_URL));
        return UserFactory.newUserWithGroupsFromJsonPayload(userAsString, groupsAsString);
    }
}
