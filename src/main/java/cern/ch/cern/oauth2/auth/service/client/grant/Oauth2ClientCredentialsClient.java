/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.cern.oauth2.auth.service.client.grant;

/**
 *
 * @author lurodrig
 */
public class Oauth2ClientCredentialsClient {

    private static final String CLIENT_ID = "CLIENT_ID";
    private static final String CLIENT_SECRET = "CLIENT_SECRET";
    private static final String AUTHZ_CODE_URL = "AUTHZ_CODE_URL";
    private static final String AUTHZ_TOKEN_URL = "AUTHZ_TOKEN_URL";
    private static final String REDIRECT_URI = "REDIRECT_URI";
    
    private static Oauth2ClientCredentialsClient instance;
    
    private String client_id;
    private String client_secret;
    private String redirect_uri;
    private String authzCodeURL;
    private String authzTokenURL;

    private Oauth2ClientCredentialsClient() {
        this.client_id = System.getenv(CLIENT_ID);
        this.client_secret = System.getenv(CLIENT_SECRET);
        this.redirect_uri = System.getenv(REDIRECT_URI);
        this.authzCodeURL = System.getenv(AUTHZ_CODE_URL);
        this.authzTokenURL = System.getenv(AUTHZ_TOKEN_URL);
    }

    public static Oauth2ClientCredentialsClient getInstance() {
        if (instance == null) {
            synchronized (Oauth2ClientCredentialsClient.class) {
                if (instance == null) {
                    instance = new Oauth2ClientCredentialsClient();
                }
            }
        }
        return instance;
    }

    public String getClient_id() {
        return client_id;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public String getRedirect_uri() {
        return redirect_uri;
    }

    public String getAuthzCodeURL() {
        return authzCodeURL;
    }

    public String getAuthzTokenURL() {
        return authzTokenURL;
    }

    
}
