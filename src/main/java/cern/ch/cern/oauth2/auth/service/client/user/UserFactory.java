/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.cern.oauth2.auth.service.client.user;

import cern.ch.cern.oauth2.auth.service.client.json.JsonUtils;

/**
 *
 * @author lurodrig
 */
public class UserFactory {

    private static final String EMAIL = "email";
    private static final String FEDERATION = "federation";
    private static final String FIRST_NAME = "first_name";
    private static final String ID = "id";
    private static final String IDENTITYCLASS = "identityclass";
    private static final String LAST_NAME = "last_name";
    private static final String MOBILE = "mobile";
    private static final String NAME = "name";
    private static final String PERSONID = "personid";
    private static final String PHONE = "phone";
    private static final String USERNAME = "username";

    public static UserWithGroups newUserWithGroupsFromJsonPayload(String userAsString, String groupsAsString) {
        UserWithGroups userWithGroups = new UserWithGroups();
        userWithGroups.setEmail(JsonUtils.get(userAsString, EMAIL));
        userWithGroups.setFederation(JsonUtils.get(userAsString, FEDERATION));
        userWithGroups.setFirst_name(JsonUtils.get(userAsString, FIRST_NAME));
        userWithGroups.setGroups(null);
        userWithGroups.setId(JsonUtils.getInt(userAsString, ID));
        userWithGroups.setIdentityclass(JsonUtils.get(userAsString, IDENTITYCLASS));
        userWithGroups.setLast_name(JsonUtils.get(userAsString, LAST_NAME));
        userWithGroups.setMobile(JsonUtils.get(userAsString, MOBILE));
        userWithGroups.setName(JsonUtils.get(userAsString, NAME));
        userWithGroups.setPersonid(JsonUtils.getInt(userAsString, PERSONID));
        userWithGroups.setPhone(JsonUtils.get(userAsString, PHONE));
        userWithGroups.setUsername(JsonUtils.get(userAsString, USERNAME));
        return userWithGroups;
    }
}
