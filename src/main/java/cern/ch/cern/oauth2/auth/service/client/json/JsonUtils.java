/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cern.ch.cern.oauth2.auth.service.client.json;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

/**
 *
 * @author lurodrig
 */
public class JsonUtils {

    public static JsonObject getJsonObject(String string) {
        InputStream is = new ByteArrayInputStream(string.getBytes());
        JsonObject jsonObject;
        try (JsonReader jsonReader = Json.createReader(is)) {
            jsonObject = jsonReader.readObject();
        }
        return jsonObject;
    }

    public static JsonArray getJsonArray(String string) {
        InputStream is = new ByteArrayInputStream(string.getBytes());
        JsonArray jsonArray;
        try (JsonReader jsonReader = Json.createReader(is)) {
            jsonArray = jsonReader.readArray();
        }
        return jsonArray;
    }

    public static String get(String string, String key) {
        String value = null;
        JsonObject jsonObject = getJsonObject(string);
        if (!jsonObject.isNull(key)) {
            value = jsonObject.getString(key);
        }
        return value;
    }

    public static int getInt(String string, String key) {
        int value = 0;
        JsonObject jsonObject = getJsonObject(string);
        if (!jsonObject.isNull(key)) {
            value = jsonObject.getInt(key);
        }
        return value;
    }

    public static String get(JsonObject jsonObject, String key) {
        String value = null;
        if (!jsonObject.isNull(key)) {
            value = jsonObject.getString(key);
        }
        return value;
    }

    public static String get(JsonArray jsonArray, String key) {
        String value = null;
        for (JsonValue jsonValue : jsonArray) {
            JsonObject jsonObject = (JsonObject) jsonValue;
            String type = jsonObject.getString("Type");
            if (type != null && type.equals(key)) {
                value = jsonObject.getString("Value");
            }
        }
        return value;
    }

}
