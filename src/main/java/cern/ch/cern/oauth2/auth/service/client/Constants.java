package cern.ch.cern.oauth2.auth.service.client;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lurodrig
 */
public class Constants {
    public static final String AUTHENTICATED_USER = "AUTHENTICATED_USER";
    public static final String COOKIE_NAME = "COOKIE_NAME";
    public static final String OAUTH2_CLIENT_URL = "OAUTH2_CLIENT_URL";
    public static final String RETURN_PATH = "return_path";
    public static final String CODE = "code";
    public static final String STATE = "state";
}
