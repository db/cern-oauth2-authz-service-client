# ​OAuth 2 Authorization Service Java Demo Client

## What?

Sample Java application for demo the [OAuth 2 Authorization Service Java Demo Client](https://espace.cern.ch/authentication/CERN%20Authentication/OAuth.aspx). 

It does not make use of any OAUTH2 java libraries like [apache oltu](http://oltu.apache.org/) or [spring security](https://projects.spring.io/spring-security-oauth/). If you think that you can need one, this [website](https://oauth.net/code/) has a complete list.
 
This example implements the [client credentials grant](https://tools.ietf.org/html/rfc6749#section-1.3.4). This is the simplest of all the 4 OAUTH2 grants. It is suitable for machine-to-machine authentication where a specific user’s permission to access data is not required. This means that the requested resources are owned by the application itself. The simplified flow would look like this:

````
  +--------+                                           +---------------+
  |        |--(A)------- Authorization Grant --------->|               |
  |        |                                           |               |
  |        |<-(B)----------- Access Token -------------|               |
  |        |               & Refresh Token             |               |
  |        |                                           |               |
  |        |                            +----------+   |               |
  |        |--(C)---- Access Token ---->|          |   |               |
  |        |                            |          |   |               |
  |        |<-(D)- Protected Resource --| Resource |   | Authorization |
  | Client |                            |  Server  |   |     Server    |
  |        |                            +----------+   |               |
  +--------+                                           +---------------+
````

[CERN Authentication](https://espace.cern.ch/authentication/default.aspx) proposes this service as an alternative for the [Shibboleth](https://espace.cern.ch/authentication/CERN%20Authentication/Configure%20a%20Shibboleth%20Application.aspx) or [SAML2](https://espace.cern.ch/authentication/CERN%20Authentication/Configure%20a%20SAML2%20Application.aspx) profiles.

In this service the roles would be distribute like this:

- **Client**: our sample java application
- **Resource Server**: https://oauthresource.web.cern.ch/api/
- **Authorization Server**: https://oauth.web.cern.ch/OAuth/

And the sequence of calls would look like this 

## How?

## Where?

## References

[Alex Bilbie](https://alexbilbie.com/guide-to-oauth-2-grants/)


  